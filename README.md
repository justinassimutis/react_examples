NEXT:

 - ES6
 - REDUX
 - FLUX   ACTIVE
 - WEBPACK 2
 - Test
 - pagination

FLUX
- Integrate FLUX
	- stores

PLAN:
 - one feature, one react component

MONGO CRUD:
 + Create NEW MODEL
 	+ NEW SCHEMA for TODOS
 + Render TODOS in page
 + schema TODO id

TODO LIST
 + Form
 + rendering on page
 + save todo


 SERVER
	+ LIVE RELOAD
		+ on LESS change
		+ on Jade change
NOTES: live reload is not anymore separate, it is now included in GRUNT watch.
What it does: it creates a server on the specified port where the events are emitted so other applications can listen to it.
Only drawback you need to include link in the webpage manually with the URL port and path to livereload.js where it will download it will communicate with reload server via WEBSOCKETS.

JADE
 + support
 + change evs templates to jade

HTTPS
 - add HTPPS secure cookie
 + generate SSL keys
 + https server.js config

REACT:
	+ forEach Example (menu added with for EachExample)

COMPONENTS:
	+ Top Menu
		+ add styles

ROUTER
+ Integrate ROUTER


LESS
+ Less files integration:
	+ so it renders before CSS    SERVER
	+ so you can use variables		SERVER
	+ watch so it renders CSS on Less file change    SERVER
