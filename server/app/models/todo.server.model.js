var mongoose = require('mongoose'),
	crypto = require('crypto'),
	Schema = mongoose.Schema;

var todo = new Schema({
	username: String,
	task: String,
	category: String,
	parent: String,
	notes: String
});


mongoose.model('Todo', todo);
