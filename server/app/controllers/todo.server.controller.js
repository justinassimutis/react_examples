var Todo = require('mongoose').model('Todo'),
	passport = require('passport');



exports.create = function(req, res, next) {
	//console.log("-----todo--create---", req.user);
	req.body.username = req.user.username;
	//console.log("--todo--create-body--", req.body.username);
	var todo = new Todo(req.body);
	todo.save(function(err) {
		if (err) {
			return next(err);
		}
		return res.redirect('/');
	});
};

exports.list = (username) => {
	// return here because we will expect to return promise
	//on the other end
	return Todo.find({username}, (err, todosList) => {
		if (err) {
			return next(err);
		}
		
		return todosList || [];

	});
};

exports.read = function(req, res) {
	res.json(req.todo);
};

exports.todoByID = function(req, res, next, id) {
	Todo.findOne({
			_id: id
		},
		function(err, todo) {
			if (err) {
				return next(err);
			}
			else {
				req.todo = todo;
				next();
			}
		}
	);
};

exports.update = function(req, res, next) {
	Todo.findByIdAndUpdate(req.todo.id, req.body, function(err, todo) {
		if (err) {
			return next(err);
		}
		else {
			res.json(todo);
		}
	});
};

exports.delete = function(req, res, next) {
	var id = req.path.split("/")[3];

	Todo.remove({ _id: id }, function(err) {
	    if (err) {
	            throw(err)
	    }
	    res.redirect('/');
	});
};
