var todoModel = require("./todo.server.controller");

exports.render = function(req, res, next) {

	//console.log("--index--contr----req.user----", req.user);

	if (req.user) {
		todoModel.list(req.user.username).then(function (todoList) {
			res.render('index', {
		    	title: 'MANO EXPRESS',
		    	user: req.user ? req.user.username : '',
		    	todoList: todoList
		    });
		    next();
		    return;
		});
	} else {
		res.render('index', {
	    	title: 'MANO EXPRESS',
	    	user: '',
	    	todoList: []
	    });
	    next();
	}
};
