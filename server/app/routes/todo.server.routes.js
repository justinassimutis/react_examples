var todo = require('../../app/controllers/todo.server.controller'),
	passport = require('passport');

module.exports = function(app) {
	app.route('/todo').post(todo.create).get(todo.list);
	app.route('/todo/:todoId').get(todo.read).put(todo.update).delete(todo.delete);
	app.route('/todo/delete/:todoId').post(todo.delete);
};
