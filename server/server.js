process.env.NODE_ENV = process.env.NODE_ENV || 'development';

var config = require('./config/config'),
	mongoose = require('./config/mongoose'),
	express = require('./config/express'),
	passport = require('./config/passport'),
	https = require('https'),
	fs = require('fs');

var privateKey  = fs.readFileSync('./config/keys/server.key', 'utf8');
var certificate = fs.readFileSync('./config/keys/server.crt', 'utf8');

var credentials = {key: privateKey, cert: certificate};

var db = mongoose(),
	app = express(),
	passport = passport();

//app.listen(config.port);
var httpsServer = https.createServer(credentials, app);
httpsServer.listen(config.port);
//delete this comment


module.exports = app;
console.log(process.env.NODE_ENV + ' server running at https://localhost:' + config.port);
