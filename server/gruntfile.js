module.exports = function(grunt) {

	// Project configuration.
	grunt.initConfig({
		concurrent: {
			dev: {
				tasks: ["less", "nodemon", "watch"],
				options: {
					logConcurrentOutput: true
				}
			}
		},
		less: {
			development: {
				files: {
					'public/css/server.css': 'styles/less/server.less'
				}
			}
		},
		watch: {
			options: {
				livereload: {
					host: "localhost",
					port: 9000,
					key: grunt.file.read("./config/keys/server.key"),
					cert: grunt.file.read("./config/keys/server.crt")
				}
			},
			styles: {
				files: ['**/*.less'],
				tasks: ["less"],
			},
			server: {
				files: [".rebooted"]
			}
		},
		nodemon: {
			dev: {
				script: "server.js",
				options: {
					nodeArgs: ['--debug'],
					ext: "js, jade",
					watch: ["app"],
					callback: function (nodemon) {
						nodemon.on('restart', function () {
							// Delay before server listens on port
							setTimeout(function() {
								require('fs').writeFileSync('.rebooted', 'rebooted');
							}, 1000);
						});
					}
				}
			}
		}
	});

	// Load the plugin that provides the "uglify" task.
	grunt.loadNpmTasks('grunt-contrib-less');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-nodemon');
	grunt.loadNpmTasks('grunt-concurrent');

	// Default task(s).
	grunt.registerTask('default', ['concurrent']);

};
