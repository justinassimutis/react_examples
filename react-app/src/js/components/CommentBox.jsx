import React from "react";

export default class CommentBox extends React.Component {
  render() {
    return(
      <div className="comment-box">
        <h3>Comments</h3>
        <h4 className="comment-count">2 comments</h4>
        <div className="comment-list">
          <Comment author="Anne droid" body="I want to know what love is..."/>
        </div>
      </div>
    );
  }
}
