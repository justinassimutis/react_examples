import React from "react";
import classNames from "classNames";
import styles from "../../css/app.css"

export default class Button extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      "on": true
    };

    console.log(styles);
  }

  render() {
    let classes = classNames(this.state.on ? styles.green : styles.red, {
      "blue": this.state.on,
      "other": !this.state.on,
      "rounded": true
    });

    return (
      <button
        className={classes}
        onClick={this.onClick.bind(this)}>
        {this.state.on ? "ON" : "OFF"} somess
       </button>
    )
  }

  onClick(e) {
    e.preventDefault();
    this.setState({
      "on": !this.state.on
    });
  }
}

