import React from 'react';
import {Link} from "react-router";


export default class Menu extends React.Component {
	constructor(props) {
		super(props);
	}

	render() {
		return (
			<nav className="nav">
				<ul>
					<li className="listItem">
						<Link to="/">Home</Link>
					</li>
					<li className="listItem">
						<Link to="/about">about us</Link>
					</li>
					<li className="listItem">
						<Link to="/contact">contac</Link>
					</li>
				</ul>
			</nav>
		);
	}
}
