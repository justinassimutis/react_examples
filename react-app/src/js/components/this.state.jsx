import React from "react";

export default class Comment extends React.Component {
	constructor() {
		super();
		this.state = {
			isAbusive: false
		};
	}

	render() {
		let commentBody;

		if (!this.state.isAbusive) {
			commentBody = this.props.body;
		} else {
			commentBody = <em> Content marked as abusive </em>;
		}

		return (
			<div className = "comment" >
				<img
					src = { this.props.avatarUrl }
					alt = { `${this.props.author}'s picture`}
				/>
				<p className = "comment-header" > { this.props.author } </p>
				<p className = "comment-body" > ` {commentBody} </p>
				<div className="comment-actions">
  					<a href="#">Delete comment</a>
  					<a href="#">Report as Abuse</a>
				</div>
			</div>
		);
	}
}
