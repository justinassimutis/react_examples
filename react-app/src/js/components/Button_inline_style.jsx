import React from "react";

export default function Button({
	active,
	disabled,
	children,
	onClick
}) {
	return (
		<button
			onClick={() => disabled || onClick()}
			disabled={disabled || active}
			style={active ? { backgroundColor: "blue", color: "white" } : {}} >
			{children}
		</button>
	);
}
