import React from "react";
import {render} from "react-dom";
import { Router, Route, Link, browserHistory } from "react-router";

import Layout from "./Layout";
import Footer from "./components/Footer";
import Button from "./components/Button";

render((
  <Router history={browserHistory}>
    <Route path="/" component={Layout}/>
    <Route path="about" component={Footer}/>
    <Route path="contact" component={Button}/>
  </Router>
), document.getElementById('app'));

