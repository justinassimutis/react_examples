import React from "react";

import Footer from "./components/Footer";
import Header from "./components/Header";
import Form from "./components/Form";
import Button from "./components/Button";
import CommentBox from "./components/CommentBox_dynamic";
import Menu from "./components/menu/Menu";

export default class Layout extends React.Component {
  constructor() {
    super();
    this.state = {
      title: "World-check",
    };
  }

  changeTitle(title) {
    this.setState({title});
  }

  render() {
    return (
      <div>
        <Menu />
        <Header changeTitle={this.changeTitle.bind(this)} title={this.state.title} />
        <Form />
        <Footer />
        <Button />
        <CommentBox />
      </div>
    );
  }
}
